﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecordTrackingApplication.Utilities
{
    public static class Extensions
    {
        /// <summary>
        /// Abbreviates <paramref name="input"/> by taking the first letter from each substring.
        /// </summary>
        /// <param name="input">The <see cref="string"/> to be abbreviated</param>
        /// <returns>A <see cref="string"/> abbreviation</returns>
        static public string ToAbbreviation(this string input)
        {
            return new string(input.Split().Select(subString => subString.First()).ToArray());
        }
    }
}
