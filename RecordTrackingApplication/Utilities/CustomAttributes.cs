﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RecordTrackingApplication.Utilities
{
    public class RequiredIfAttribute : ValidationAttribute
    {

        RequiredAttribute _innerAttribute = new RequiredAttribute();
        protected string dependentProperty { get; set; }
        protected object targetValue { get; set; }

        public RequiredIfAttribute(string dependentProperty, object targetValue)
        {
            this.dependentProperty = dependentProperty;
            this.targetValue = targetValue;
        }
        protected override ValidationResult IsValid(object value, ValidationContext validationContext)
        {
            var field = validationContext.ObjectType.GetProperty(dependentProperty);
            if (field != null)
            {
                var dependentValue = field.GetValue(validationContext.ObjectInstance, null);
                if ((dependentValue == null && targetValue == null) || dependentValue.Equals(targetValue))
                {
                    if (!_innerAttribute.IsValid(value))
                    {
                        string name = validationContext.DisplayName;
                        return new ValidationResult(ErrorMessage = name + " is required.");
                    }
                }
                return ValidationResult.Success;
            }
            else
            {
                return new ValidationResult(FormatErrorMessage(dependentProperty));
            }
        }

    }
}
