﻿using RecordTrackingApplication.Models;
using RecordTrackingApplication.Models.HeroesOfTheStorm;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecordTrackingApplication.Data
{
    public static class DBInitialiser
    {
        private static Random randomTimeGenerator = new Random();

        static DateTimeOffset GetRandomDateTime(this DateTimeOffset dateTimeOffset) => DateTimeOffset.FromUnixTimeSeconds(randomTimeGenerator.Next(0, (int)dateTimeOffset.ToUnixTimeSeconds()));

        public static bool Initialise(RecordContext context)
        {
            if (context.Records.Any())
            {
                return false;
            }

            var heroes = new Hero[]
            {
                new Hero(){ Name = Heroes.Abathur, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Alarak, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Alexstrasza, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Ana, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Anduin, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Anub_arak, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Artanis, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Arthas, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Auriel, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Azmodan, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Blaze, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Brightwing, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Cassia, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Chen, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Cho, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Cho_gall, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Chromie, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Deathwing, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Deckard, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Dehaka, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Diablo, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.DVa, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.ETC, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Falstad, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Fenix, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Gall, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Garrosh, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Gazlowe, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Genji, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Greymane, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Gul_dan, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Hanzo, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Hogger, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Illidan, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Imperius, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Jaina, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Johanna, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Junkrat, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Kael_thas, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Kel_Thuzad, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Kerrigan, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Kharazim, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Leoric, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Li_Ming, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.LiLi, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.LtMorales, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Lucio, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Lunara, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Maiev, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Mal_Ganis, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Malfurion, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Malthael, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Medivh, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Mei, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Mephisto, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Muradin, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Murky, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Nazeebo, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Nova, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Orphea, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Probius, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Qhira, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Ragnaros, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Raynor, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Rehgar, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Rexxar, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Samuro, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Sgt_Hammer, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Sonya, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Stitches, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Stukov, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Sylvanas, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Tassadar, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.TheButcher, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.TheLostVikings, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Thrall, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Tracer, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Tychus, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Tyrael, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Tyrande, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Uther, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Valeera, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Valla, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Varian, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Whitemane, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Xul, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Yrel, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Zagara, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Zarya, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Zeratul, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
                new Hero(){ Name = Heroes.Zul_jin, BlizzardUniverse = BlizzardUniverses.Warcraft, Class = HeroClasses.Tank},
            };

            context.Heroes.AddRange(heroes);
            context.SaveChanges();

            var criterias = new Criteria[]
            {
                new Criteria(){ Name = "Hero Damage"},
                new Criteria(){ Name = "Team Healing"},
                new Criteria(){ Name = "Escape Artist"}
            };

            context.Criterias.AddRange(criterias);
            context.SaveChanges();

            var datas = new CountData[]
            {
                new CountData(){ Value = 69420,
                    Criteria = criterias.Single(c => c.Name == "Escape Artist"),
                    Hero = heroes.Single(h => h.Name == Heroes.Abathur),
                    EntryTime = DateTimeOffset.Now.GetRandomDateTime()},

                new CountData(){ Value = 99999,
                    Criteria = criterias.Single(c => c.Name == "Hero Damage"),
                    Hero = heroes.Single(h => h.Name == Heroes.Alarak),
                    EntryTime = DateTimeOffset.Now.GetRandomDateTime()},

                new CountData(){ Value = 1,
                    Criteria = criterias.Single(c => c.Name == "Hero Damage"),
                    Hero = heroes.Single(h => h.Name == Heroes.Alarak),
                    EntryTime = DateTimeOffset.Now.GetRandomDateTime()},

                new CountData(){ Value = 343,
                    Criteria = criterias.Single(c => c.Name == "Team Healing"),
                    Hero = heroes.Single(h => h.Name == Heroes.ETC),
                    EntryTime = DateTimeOffset.Now.GetRandomDateTime()}
            };

            context.Datas.AddRange(datas);
            context.SaveChanges();

            var records = new Record[]
            {
                new Record{Hero = heroes.Single(h => h.Name == Heroes.Alarak), ProperRecordName = "Sadism", RecordType = RecordTypes.QuestCountMax },
                new Record{Hero = heroes.Single(h => h.Name == Heroes.ETC), ProperRecordName = "Team Healing", RecordType = RecordTypes.MaxStat }

            };

            context.Records.AddRange(records);
            context.SaveChanges();

            var recordEntries = new RecordEntry[]
            {
                new RecordEntry{NameOfHolder = "Mugy", EntryTime = DateTimeOffset.Now, Record = records.First(record => record.ProperRecordName == "Sadism"), RecordValueType = RecordValueTypes.Count, ValueCount = 69420},
                new RecordEntry{NameOfHolder = "Mugy", EntryTime = DateTimeOffset.Now.AddHours(1).AddSeconds(1), Record = records.First(record => record.ProperRecordName == "Sadism"), RecordValueType = RecordValueTypes.Count, ValueCount = 42069},
                new RecordEntry{NameOfHolder = "Mugy", EntryTime = DateTimeOffset.Now.AddHours(2).AddSeconds(2), Record = records.First(record => record.ProperRecordName == "Sadism"), RecordValueType = RecordValueTypes.Count, ValueCount = 9001},
                new RecordEntry{NameOfHolder = "Mugy", EntryTime = DateTimeOffset.Now.AddHours(3).AddSeconds(3), Record = records.First(record => record.ProperRecordName == "Sadism"), RecordValueType = RecordValueTypes.Count, ValueCount = 0},
                new RecordEntry{NameOfHolder = "Mugy", EntryTime = DateTimeOffset.Now.AddHours(4).AddSeconds(4), Record = records.First(record => record.ProperRecordName == "Team Healing"), RecordValueType = RecordValueTypes.Count, ValueCount = 0}

            };

            context.RecordEntries.AddRange(recordEntries);
            context.SaveChanges();

            return true;
        }
    }
}
