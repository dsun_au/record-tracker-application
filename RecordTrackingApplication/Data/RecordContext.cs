﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using RecordTrackingApplication.Models;
using RecordTrackingApplication.Models.HeroesOfTheStorm;


namespace RecordTrackingApplication.Data
{
    public class RecordContext : DbContext
    {
        public RecordContext (DbContextOptions<RecordContext> options)
            : base(options)
        {
        }
        public DbSet<Hero> Heroes { get; set; }
        public DbSet<Quest> Quests { get; set; }

        public DbSet<Criteria> Criterias { get; set; }

        public DbSet<QuestCriteria> QuestCriterias { get; set; }
        public DbSet<CountQuestCriteria> CountQuestCriterias { get; set; }
        public DbSet<TimeQuestCriteria> TimeQuestCriterias { get; set; }

        public DbSet<Models.HeroesOfTheStorm.Data> Datas { get; set; }
        public DbSet<CountData> CountDatas { get; set; }
        public DbSet<TimeData> TimeDatas { get; set; }
        public DbSet<QuestCountData> QuestCountDatas { get; set; }
        public DbSet<QuestTimeData> QuestTimeDatas { get; set; }
        public DbSet<QuestCountCompletionTimeData> QuestCountCompletionTimeDatas { get; set; }
        public DbSet<QuestTimeCompletionTimeData> QuestTimeCompletionTimeDatas { get; set; }

        public DbSet<Record> Records { get; set; }
        public DbSet<RecordEntry> RecordEntries { get; set; }


        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Record>().ToTable("Record");
            modelBuilder.Entity<RecordEntry>().ToTable("RecordEntry");
        }
    }
}
