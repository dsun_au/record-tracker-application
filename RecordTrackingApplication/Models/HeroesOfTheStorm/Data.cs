﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RecordTrackingApplication.Models.HeroesOfTheStorm
{
    public abstract class Data
    {
        public int DataId { get; set; }
        public DateTimeOffset EntryTime { get; set; }


        public int CriteriaId { get; set; }
        [Required]
        public Criteria Criteria { get; set; }
        public int HeroId { get; set; }
        public Hero Hero { get; set; }
    }

    public class CountData : Data
    {
        public double Value { get; set; }
    }

    public class TimeData : Data
    {
        public TimeSpan Value { get; set; }
    }

    public class QuestCountData : CountData
    {
        // Navigation Properties
        public int QuestCriteriaId { get; set; }
        public QuestCriteria QuestCriteria { get; set; }
    }

    public class QuestCountCompletionTimeData : QuestCountData
    {
        public TimeSpan CompletionTime { get; set; }
    }

    public class QuestTimeData : TimeData
    {
        // Navigation Properties
        public int QuestCriteriaId { get; set; }
        public QuestCriteria QuestCriteria { get; set; }
    }

    public class QuestTimeCompletionTimeData : QuestTimeData
    {
        public TimeSpan CompletionTime { get; set; }
    }
}
