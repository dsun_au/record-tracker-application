﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RecordTrackingApplication.Models.HeroesOfTheStorm
{
    public class Quest
    {
        public int QuestId { get; set; }
        [Required]
        public string Name { get; set; }


        public int HeroId { get; set; }
        public Hero Hero { get; set; }

        public IEnumerable<QuestCriteria> QuestCriteria { get; set; }
    }

    public class Criteria
    {
        public int CriteriaId { get; set; }
        [Required]
        public string Name { get; set; }

        public IEnumerable<Data> Data { get; set; }
    }

    public abstract class QuestCriteria : Criteria
    {
        [Required]
        public string ValueName { get; set; }

        public int QuestId { get; set; }
        public Quest Quest { get; set; }

    }

    public class TimeQuestCriteria : QuestCriteria
    {
        [Required]
        public TimeSpan Value { get; set; }
        public TimeSpan ValueLimit { get; set; }
        public TimeSpan CompletionValue { get; set; }

        //public IEnumerable<QuestTimeData> QuestTimeDatas { get; set; }
        //public IEnumerable<QuestTimeCompletionTimeData> QuestTimeCompletionTimeDatas { get; set; }

    }

    public class CountQuestCriteria : QuestCriteria
    {
        [Required]
        public double Value { get; set; }
        public double ValueLimit { get; set; }
        public double CompletionValue { get; set; }

        //public IEnumerable<QuestCountData> QuestCountDatas { get; set; }
        //public IEnumerable<QuestCountCompletionTimeData> QuestCountCompletionTimeDatas { get; set; }
    }


}
