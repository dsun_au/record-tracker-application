﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RecordTrackingApplication.Models
{
    public enum RecordValueTypes
    {
        Count,
        Time,
    }

    public enum RecordTypes
    {
        [Display(Name = "Highest Quest Count")]
        QuestCountMax,
        [Display(Name = "Lowest Quest Count")]
        QuestCountMin,
        [Display(Name = "Highest Quest Completion Time")]
        QuestCompletionTimeMax,
        [Display(Name = "Lowest Quest Completion Time")]
        QuestCompletionTimeMin,
        [Display(Name = "Highest Statistic Value")]
        MaxStat,
        [Display(Name = "Lowest Statistic Value")]
        MinStat
    }
}
