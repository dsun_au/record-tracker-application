﻿using RecordTrackingApplication.Utilities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace RecordTrackingApplication.Models
{
    public class RecordEntry : IComparable<RecordEntry>
    {
        public int RecordEntryId { get; set; }

        [Display(Name = "Player"), Required]
        public string NameOfHolder { get; set; }

        [Display(Name = "Time of Entry")]
        public DateTimeOffset EntryTime { get; set; }

        [Display(Name = "Record Value Type")]
        public RecordValueTypes RecordValueType { get; set; }

        [Display(Name = "Count"), RequiredIf(nameof(RecordValueType), RecordValueTypes.Count)]
        public int? ValueCount { get; set; }

        [Display(Name = "Time"), RequiredIf(nameof(RecordValueType), RecordValueTypes.Time)]
        public TimeSpan? ValueTime { get; set; }

        [Display(Name = "Replay Name")]
        public string ReplayName { get; set; }

        [Display(Name = "Replay Hash")]
        public string ReplayHash { get; set; }

        // Navigation
        [Display(Name = "Record")]
        public int RecordId { get; set; }

        public Record Record { get; set; }

        public int CompareTo(RecordEntry other)
        {
            return RecordValueType switch
            {
                RecordValueTypes.Count => ((int)ValueCount).CompareTo(other.ValueCount),
                RecordValueTypes.Time => ((TimeSpan)ValueTime).CompareTo(other.ValueTime),
                _ => throw new NotImplementedException("Unknown Record Value Type"),
            };
        }
    }
}
