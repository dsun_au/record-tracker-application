﻿using RecordTrackingApplication.Models.HeroesOfTheStorm;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace RecordTrackingApplication.Models
{
    public class Record
    {
        public int RecordId { get; set; }

        public Hero Hero { get; set; }

        private string _recordName;
        [Display(Name = "Record Name")]
        public string RecordName
        {
            get { return string.IsNullOrEmpty(_recordName) ? ProperRecordName : _recordName; }
            set { _recordName = value; }
        }

        [Display(Name = "Proper Record Name"), Required]
        public string ProperRecordName { get; set; }
        [Display(Name = "Type of Record")]
        public RecordTypes RecordType { get; set; }

        // Access Properties
        [NotMapped, Display(Name = "Current Best Record")]
        public RecordEntry BestRecordEntry
        {
            get
            {
                if (RecordEntries != null)
                    return RecordType switch
                    {
                        RecordTypes.MaxStat or RecordTypes.QuestCompletionTimeMax or RecordTypes.QuestCountMax => RecordEntries.Max(),
                        RecordTypes.MinStat or RecordTypes.QuestCompletionTimeMin or RecordTypes.QuestCountMin => RecordEntries.Min(),
                        _ => throw new NotImplementedException("Unknown Record Type"),
                    };
                else
                    return null;
            }
        }

        [NotMapped]
        public string FriendlyName => $"{Hero} - {RecordName}";

        // Navigation.
        [Required]
        public ICollection<RecordEntry> RecordEntries { get; set; }

    }
}
