﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models;

namespace RecordTrackingApplication.Pages.RecordEntries
{
    public class EditModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;

        public EditModel(RecordTrackingApplication.Data.RecordContext context)
        {
            _context = context;
        }

        [BindProperty]
        public RecordEntry RecordEntry { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            RecordEntry = await _context.RecordEntries
                .Include(r => r.Record)
                .ThenInclude(r => r.Hero)
                .FirstOrDefaultAsync(m => m.RecordEntryId == id);

            if (RecordEntry == null)
            {
                return NotFound();
            }
            ViewData["HeroId"] = new SelectList(_context.Heroes, "HeroId", "Name");
            ViewData["RecordId"] = new SelectList(_context.Records, "RecordId", "FriendlyName");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(int id)
        {
            RecordEntry recordEntryToUpdate = await _context.RecordEntries.FindAsync(id);

            if (recordEntryToUpdate == null)
                return NotFound();

            if (await TryUpdateModelAsync(
                recordEntryToUpdate,
                nameof(RecordEntry),
                s => s.NameOfHolder, s => s.EntryTime, s => s.RecordValueType, s => s.ValueCount, s => s.ValueTime, s => s.RecordId))
            {
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index");
            }

            return Page();
        }
    }
}
