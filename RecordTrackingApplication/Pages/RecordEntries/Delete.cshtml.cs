﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models;

namespace RecordTrackingApplication.Pages.RecordEntries
{
    public class DeleteModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;

        public DeleteModel(RecordTrackingApplication.Data.RecordContext context)
        {
            _context = context;
        }

        [BindProperty]
        public RecordEntry RecordEntry { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            RecordEntry = await _context.RecordEntries
                .Include(r => r.Record)
                .ThenInclude(r => r.Hero)
                .FirstOrDefaultAsync(m => m.RecordEntryId == id);

            if (RecordEntry == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            RecordEntry = await _context.RecordEntries.FindAsync(id);

            if (RecordEntry != null)
            {
                _context.RecordEntries.Remove(RecordEntry);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
