﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models;

namespace RecordTrackingApplication.Pages.RecordEntries
{
    public class CreateModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;

        public CreateModel(RecordTrackingApplication.Data.RecordContext context)
        {
            _context = context;
        }

        public IActionResult OnGet(int? recordId)
        {
            ViewData["RecordId"] = new SelectList(_context.Records, "RecordId", "FriendlyName", recordId);

            return Page();
        }

        [BindProperty]
        public RecordEntry RecordEntry { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public IActionResult OnPost()
        {
            // Only need two of the fields valid as we are only editing 2 for now
            ModelState.MaxAllowedErrors = ModelState.Values.Count() - 2;

            if (ModelState.IsValid || ModelState.ErrorCount <= ModelState.MaxAllowedErrors)
                return RedirectToPage("./CreateDetailed", new { recordValueType = RecordEntry.RecordValueType, recordId = RecordEntry.RecordId});
            else
                return Page();
        }
    }

}
