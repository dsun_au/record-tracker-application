﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models;

namespace RecordTrackingApplication.Pages.RecordEntries
{
    public class CreateDetailedModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;

        public CreateDetailedModel(RecordTrackingApplication.Data.RecordContext context)
        {
            _context = context;
        }

        [BindProperty]
        public RecordEntry RecordEntry { get; set; }

        [BindProperty]
        public bool CountValueInputDisabled { get; set; } = false;

        [BindProperty]
        public bool TimeValueInputDisabled { get; set; } = false;

        public IActionResult OnGet(RecordValueTypes recordValueType, int recordId)
        {
            RecordEntry = new RecordEntry() { RecordValueType = recordValueType, RecordId = recordId, EntryTime = DateTimeOffset.Now};

            ViewData["RecordId"] = new SelectList(_context.Records, "RecordId", "FriendlyName");

            switch (recordValueType)
            {
                case RecordValueTypes.Count:
                    TimeValueInputDisabled = true;
                    break;
                case RecordValueTypes.Time:
                    CountValueInputDisabled = true;
                    break;
                default:
                    throw new NotImplementedException($"Unknown {nameof(RecordValueTypes)}");
            }

            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            var newRecordEntry = new RecordEntry();

            

            if (await TryUpdateModelAsync(
                newRecordEntry,
                nameof(RecordEntry),   // Prefix for form value.
                s => s.NameOfHolder, s => s.EntryTime, s => s.RecordValueType, s => s.ValueCount, s => s.ValueTime, s => s.RecordId))
            {
                // We'll be storing in UTC
                newRecordEntry.EntryTime = newRecordEntry.EntryTime.ToUniversalTime();
                _context.RecordEntries.Add(newRecordEntry);
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index");
            }

            return OnGet(RecordEntry.RecordValueType, RecordEntry.RecordId);
        }
    }
}
