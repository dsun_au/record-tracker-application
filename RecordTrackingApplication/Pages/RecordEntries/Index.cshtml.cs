﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models;

namespace RecordTrackingApplication.Pages.RecordEntries
{
    public class IndexModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;

        public IndexModel(RecordTrackingApplication.Data.RecordContext context)
        {
            _context = context;
        }

        public IList<RecordEntry> RecordEntry { get;set; }

        public async Task OnGetAsync()
        {
            RecordEntry = await _context.RecordEntries
                .Include(re => re.Record)
                .ThenInclude(r => r.Hero)
                .ToListAsync();
        }
    }
}
