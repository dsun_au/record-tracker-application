﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models.HeroesOfTheStorm;

namespace RecordTrackingApplication.Pages.CountDatas
{
    public class CreateModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;

        public CreateModel(RecordTrackingApplication.Data.RecordContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
        ViewData["CriteriaId"] = new SelectList(_context.Criterias, "CriteriaId", "Discriminator");
            return Page();
        }

        [BindProperty]
        public CountData CountData { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.CountDatas.Add(CountData);
            await _context.SaveChangesAsync();

            return RedirectToPage("./Index");
        }
    }
}
