﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models.HeroesOfTheStorm;

namespace RecordTrackingApplication.Pages.CountDatas
{
    public class EditModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;

        public EditModel(RecordTrackingApplication.Data.RecordContext context)
        {
            _context = context;
        }

        [BindProperty]
        public CountData CountData { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CountData = await _context.CountDatas
                .Include(c => c.Criteria).FirstOrDefaultAsync(m => m.DataId == id);

            if (CountData == null)
            {
                return NotFound();
            }
           ViewData["CriteriaId"] = new SelectList(_context.Criterias, "CriteriaId", "Discriminator");
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync()
        {
            if (!ModelState.IsValid)
            {
                return Page();
            }

            _context.Attach(CountData).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!CountDataExists(CountData.DataId))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return RedirectToPage("./Index");
        }

        private bool CountDataExists(int id)
        {
            return _context.CountDatas.Any(e => e.DataId == id);
        }
    }
}
