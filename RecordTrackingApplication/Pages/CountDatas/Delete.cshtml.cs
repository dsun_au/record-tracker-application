﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models.HeroesOfTheStorm;

namespace RecordTrackingApplication.Pages.CountDatas
{
    public class DeleteModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;

        public DeleteModel(RecordTrackingApplication.Data.RecordContext context)
        {
            _context = context;
        }

        [BindProperty]
        public CountData CountData { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CountData = await _context.CountDatas
                .Include(c => c.Criteria).FirstOrDefaultAsync(m => m.DataId == id);

            if (CountData == null)
            {
                return NotFound();
            }
            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CountData = await _context.CountDatas.FindAsync(id);

            if (CountData != null)
            {
                _context.CountDatas.Remove(CountData);
                await _context.SaveChangesAsync();
            }

            return RedirectToPage("./Index");
        }
    }
}
