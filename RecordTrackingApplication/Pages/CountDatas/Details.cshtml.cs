﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models.HeroesOfTheStorm;

namespace RecordTrackingApplication.Pages.CountDatas
{
    public class DetailsModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;

        public DetailsModel(RecordTrackingApplication.Data.RecordContext context)
        {
            _context = context;
        }

        public CountData CountData { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            CountData = await _context.CountDatas
                .Include(c => c.Criteria).FirstOrDefaultAsync(m => m.DataId == id);

            if (CountData == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
