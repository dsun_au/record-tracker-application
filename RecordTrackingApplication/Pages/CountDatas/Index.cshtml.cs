﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models.HeroesOfTheStorm;

namespace RecordTrackingApplication.Pages.CountDatas
{
    public class IndexModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;

        public IndexModel(RecordTrackingApplication.Data.RecordContext context)
        {
            _context = context;
        }

        public IList<CountData> CountData { get;set; }

        public async Task OnGetAsync()
        {
            CountData = await _context.CountDatas
                .Include(c => c.Criteria)
                .Include(c => c.Hero)
                .ToListAsync();
        }
    }
}
