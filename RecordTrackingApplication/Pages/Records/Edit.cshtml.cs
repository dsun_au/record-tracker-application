﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models;

namespace RecordTrackingApplication.Pages.Records
{
    public class EditModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;

        public EditModel(RecordTrackingApplication.Data.RecordContext context)
        {
            _context = context;
        }

        [BindProperty]
        public Record Record { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Record = await _context.Records.FindAsync(id);

            if (Record == null)
            {
                return NotFound();
            }
            return Page();
        }

        // To protect from overposting attacks, enable the specific properties you want to bind to.
        // For more details, see https://aka.ms/RazorPagesCRUD.
        public async Task<IActionResult> OnPostAsync(int id)
        {
            Record recordToUpdate = await _context.Records.FindAsync(id);

            if (recordToUpdate == null)
                return NotFound();

            if (await TryUpdateModelAsync(
                recordToUpdate,
                nameof(Record),
                s => s.Hero, s => s.RecordName, s => s.ProperRecordName, s => s.RecordType))
            {
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index");
            }

            return Page();
        }
    }
}
