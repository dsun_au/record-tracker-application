﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models;

namespace RecordTrackingApplication.Pages.Records
{
    public class CreateModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;

        public CreateModel(RecordTrackingApplication.Data.RecordContext context)
        {
            _context = context;
        }

        public IActionResult OnGet()
        {
            _context.Records
                .Include(r => r.Hero);

            ViewData["HeroId"] = new SelectList(_context.Heroes, "HeroId", "Name");
            return Page();
        }

        [BindProperty]
        public Record Record { get; set; }

        // To protect from overposting attacks, see https://aka.ms/RazorPagesCRUD
        public async Task<IActionResult> OnPostAsync()
        {
            var emptyRecord = new Record();

            if (await TryUpdateModelAsync<Record>(
                emptyRecord,
                nameof(Record),   // Prefix for form value.
                s => s.Hero, s => s.RecordName, s => s.ProperRecordName, s => s.RecordType, s => s.RecordId))
            {
                _context.Records.Add(emptyRecord);
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index");
            }

            return OnGet();
        }
    }
}
