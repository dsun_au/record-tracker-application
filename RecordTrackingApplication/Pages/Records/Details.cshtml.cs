﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models;

namespace RecordTrackingApplication.Pages.Records
{
    public class DetailsModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;

        public DetailsModel(RecordTrackingApplication.Data.RecordContext context)
        {
            _context = context;
        }

        public Record Record { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Record = await _context.Records
                .Include(record => record.RecordEntries)
                .Include(r => r.Hero)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.RecordId == id);

            if (Record == null)
            {
                return NotFound();
            }
            return Page();
        }
    }
}
