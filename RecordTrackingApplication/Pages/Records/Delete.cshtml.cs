﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models;

namespace RecordTrackingApplication.Pages.Records
{
    public class DeleteModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;
        private readonly ILogger<DeleteModel> _logger;

        public DeleteModel(RecordTrackingApplication.Data.RecordContext context, ILogger<DeleteModel> logger)
        {
            _context = context;
            _logger = logger;
        }

        [BindProperty]
        public Record Record { get; set; }
        public string ErrorMessage { get; set; }

        public async Task<IActionResult> OnGetAsync(int? id, bool? saveChangesError = false)
        {
            if (id == null)
                return NotFound();

            Record = await _context.Records
                .Include(r => r.Hero)
                .AsNoTracking()
                .FirstOrDefaultAsync(m => m.RecordId == id);

            if (Record == null)
                return NotFound();

            if (saveChangesError.GetValueOrDefault())
                ErrorMessage = $"Delete {id} failed. Try again";

            return Page();
        }

        public async Task<IActionResult> OnPostAsync(int? id)
        {
            if (id == null)
            {
                return NotFound();
            }

            Record = await _context.Records.FindAsync(id);

            if (Record == null)
                return NotFound();


            try
            {
                _context.Records.Remove(Record);
                await _context.SaveChangesAsync();
                return RedirectToPage("./Index");
            }
            catch (DbUpdateException ex)
            {
                _logger.LogError(ex, ErrorMessage);

                return RedirectToAction("./Delete", new { id, saveChangesError = true });
            }
        }
    }
}
