﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models;

namespace RecordTrackingApplication.Pages.Records
{
    public class IndexModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;

        public IndexModel(RecordTrackingApplication.Data.RecordContext context)
        {
            _context = context;
        }

        public IList<Record> Record { get; set; }

        public async Task OnGetAsync()
        {
            Record = await _context.Records
                .Include(record => record.RecordEntries)
                .Include(r => r.Hero)
                .AsNoTracking()
                .ToListAsync();
        }
    }
}
