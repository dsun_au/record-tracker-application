﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace RecordTrackingApplication.Pages
{
    public class IndexModel : PageModel
    {
        private readonly ILogger<IndexModel> _logger;

        public IndexModel(ILogger<IndexModel> logger)
        {
            _logger = logger;
#if DEBUG
            BuildType = "Debug";
#else
            BuildType = "Release";
#endif
        }

        public void OnGet()
        {

        }

        [BindProperty]
        public string BuildType { get; set; }
    }
}
