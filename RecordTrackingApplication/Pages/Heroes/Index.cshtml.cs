﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.RazorPages;
using Microsoft.EntityFrameworkCore;
using RecordTrackingApplication.Data;
using RecordTrackingApplication.Models.HeroesOfTheStorm;

namespace RecordTrackingApplication.Pages.Heroes
{
    public class IndexModel : PageModel
    {
        private readonly RecordTrackingApplication.Data.RecordContext _context;

        public IndexModel(RecordTrackingApplication.Data.RecordContext context)
        {
            _context = context;
        }

        public IList<Hero> Hero { get;set; }

        public async Task OnGetAsync()
        {
            Hero = await _context.Heroes
                .Include(h => h.Quests)
                .Include(h => h.Datas)
                .ThenInclude(d => d.Criteria)
                .ToListAsync();
        }
    }
}
