## Introduction

This is a project where people can track interesting records in Heroes of the Storm. Eventually I hope to make record input automatically by parsing a replay.

Staging build: https://record-tracker-heroes-staging.herokuapp.com/
"Production" build: https://record-tracker-heroes.herokuapp.com/