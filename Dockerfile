# Dockerfile

FROM mcr.microsoft.com/dotnet/sdk:5.0 AS build-env
WORKDIR ./app

# Copy csproj and restore as distinct layers
COPY . .

RUN dotnet restore --verbosity m --source https://api.nuget.org/v3/index.json --packages ./nugetPackages/

RUN dotnet publish --no-restore -c Release -o out

# Build runtime image
FROM mcr.microsoft.com/dotnet/aspnet:5.0

WORKDIR ./app

COPY --from=build-env ./app/out .

CMD ASPNETCORE_URLS=http://*:$PORT /app/RecordTrackingApplication